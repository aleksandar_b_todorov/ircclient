package sample.ircbackend.user;

public class User
{
    private String nickname;
    private String username;
    private String fullName;
    private boolean isOperator;


    public User()
    {
        this.nickname = "";
        this.username = "";
        this.fullName = "";
        this.isOperator = false;
    }

    public User(String nickname, String username, String fullName)
    {
        this.nickname = nickname;
        this.username = username;
        this.fullName = fullName;
        this.isOperator = false;
    }

    public String getNickname()
    {
        return nickname;
    }

    public void setNickname(String nickname)
    {
        this.nickname = nickname;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public boolean isOperator()
    {
        return isOperator;
    }

    public void setOperator(boolean operator)
    {
        isOperator = operator;
    }
}
