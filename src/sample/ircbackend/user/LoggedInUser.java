package sample.ircbackend.user;

public class LoggedInUser {
    private User loggedInUser;

    public boolean isLoggedIn;

    private static LoggedInUser instance;

    private LoggedInUser() {}

    synchronized public static LoggedInUser getInstance()   {
        if(instance == null) {
            instance = new LoggedInUser();
        }

        return instance;
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        isLoggedIn = loggedIn;
    }

    public User getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(User loggedInUser) {
        this.loggedInUser = loggedInUser;
    }
}
