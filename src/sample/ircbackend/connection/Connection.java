package sample.ircbackend.connection;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * The connection to the server
 * At a time only 1 connection is possible(since we only need one)
 */
public class Connection
{
    private static final Logger LOGGER = Logger.getGlobal();

    private static Socket socket;

    /**
     * Instantiate the connection to the server, host and port could be hard coded since its always
     * chat.freenode.net and port - 6667
     * NOTE: changed to irc.ubuntu.net for better performance
     *
     * @param host the url of the server (irc.ubuntu.net)
     * @param port the port of the server (6667)
     * @throws IOException            If there is a problem with the connection (thrown by the socket)
     * @throws IllegalAccessException if the connection is already instantiated (not null)
     */
    public static void createConnection(String host, int port) throws IOException, IllegalAccessException
    {
        if (socket != null && !socket.isConnected())
            throw new IllegalAccessException("Connection is already established, cannot make 2 connections at a time");
        socket = new Socket(host, port);
        LOGGER.info("Connection established, socket is open");
    }

    /**
     * Get the output stream of the socket for writing to the server
     *
     * @return the output stream
     * @throws IOException            thrown by the socket
     * @throws IllegalAccessException thrown if the socket is not instantiated or connection is lost
     */
    public static OutputStream getOutputStream() throws IOException, IllegalAccessException
    {
        if (socket == null || !socket.isConnected())
            throw new IllegalAccessException("Socket is not connected, or connection is closed\n" +
                    "Maybe you didnt instantiate the Connection class?");
        return socket.getOutputStream();
    }

    /**
     * Get the input stream of the socket for reading the messages from the server
     *
     * @return the input stream
     * @throws IOException            thrown by the socket
     * @throws IllegalAccessException thrown if the socket is not instantiated or connection is lost
     */
    public static InputStream getInputStream() throws IOException, IllegalAccessException
    {
        if (socket == null || !socket.isConnected())
            throw new IllegalAccessException("Socket is not connected, or connection is closed\n" +
                    "Maybe you didnt instantiate the Connection class?");

        return socket.getInputStream();
    }

}
