package sample.ircbackend.communication.server.to;

import sample.ircbackend.connection.Connection;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.logging.Logger;

/**
 * Uses BufferedWriter to write to the socket's output stream
 * Since there is a single socket instance used for communication
 */
public class ServerWriter implements Closeable
{
    private static final Logger LOGGER = Logger.getGlobal();
    private BufferedWriter writer;

    public ServerWriter() throws IOException, IllegalAccessException
    {
        this.writer = new BufferedWriter(new OutputStreamWriter(Connection.getOutputStream()));
        LOGGER.info("Successfully instantiated ServerWriter");
    }

    public void write(String msg)
    {
        try
        {
            writer.write(msg + "\r\n");
            LOGGER.info(">>> Sent message to server: " + msg);
            writer.flush();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void write(String... messages)
    {

        String wholeMessage = String.join(" ", messages);
        wholeMessage += "\r\n";

        LOGGER.info(">>> Sent message to server: " + wholeMessage);
        this.write(wholeMessage);
    }

    @Override
    public void close() throws IOException
    {
        this.writer.close();

        LOGGER.info("Writer closed");
    }
}

