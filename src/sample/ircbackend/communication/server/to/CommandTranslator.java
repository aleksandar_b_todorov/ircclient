package sample.ircbackend.communication.server.to;

public class CommandTranslator
{
    public static final String PRIVATE_MESSAGE_COMMAND = "PRIVMSG";
    public static final String JOIN_CHANNEL_COMMAND = "JOIN";
    public static final String LEAVE_CHANNEL_COMMAND = "PART";
    public static final String QUIT_CLIENT_SESSION_COMMAND = "QUIT";
    public static final String WHO_IS_COMMAND = "WHOIS";
    public static final String SET_USER_COMMAND = "USER";
    public static final String SET_NICK_COMMAND = "NICK";
    public static final String KICK_USER = "KICK";

    public static final String SPACE = " ";
    public static final String COLON = ":";
    public static final String CHANNEL_SIGN = "#";
    public static final String STAR_SIGN = "*";
    public static final String TOPIC_COMMAND = "TOPIC";

    private static String fullMessage;

    public static String privateMessage(String receiver, String message)
    {
        fullMessage = PRIVATE_MESSAGE_COMMAND + SPACE + receiver + SPACE + COLON + message;
        return fullMessage;
    }

    public static String joinIntoChannel(String channelName)
    {
        fullMessage = JOIN_CHANNEL_COMMAND + SPACE + channelName;
        return fullMessage;
    }

    public static String leaveChannel(String channelName)
    {
        fullMessage = LEAVE_CHANNEL_COMMAND + SPACE + channelName;
        return fullMessage;
    }

    public static String quitSession(String message)
    {
        fullMessage = QUIT_CLIENT_SESSION_COMMAND + SPACE + COLON + message;
        return fullMessage;
    }

    public static String whoIs(String nickname)
    {
        fullMessage = WHO_IS_COMMAND + SPACE + nickname;
        return fullMessage;
    }

    public static String login(String nickname, String username, String realName)
    {
        fullMessage = SET_NICK_COMMAND + SPACE + nickname + "\r\n" +
                SET_USER_COMMAND + SPACE + username + SPACE + "8" + SPACE + STAR_SIGN + SPACE + COLON + SPACE + realName;
        return fullMessage;
    }
}
