package sample.ircbackend.communication.server;

import sample.ircbackend.communication.server.from.IrcEventFactory;
import sample.ircbackend.communication.server.from.Reader;
import sample.ircbackend.communication.server.from.events.api.IrcEvent;
import sample.ircbackend.communication.server.from.events.api.IrcReplyEvent;
import sample.ircbackend.communication.server.from.events.api.IrcUserActionEvent;
import sample.ircbackend.communication.server.from.events.impl.PingIrcEvent;
import sample.ircbackend.communication.server.to.ServerWriter;
import sample.ircbackend.connection.Connection;

import java.io.IOException;
import java.net.SocketException;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class handles communication with the server
 * handles the server listening thread and holds reference to the ServerWriter object
 *
 * @author Stefan Ivanov
 */
public class CommunicationService implements Runnable
{
    private static final Logger LOGGER = Logger.getGlobal();

    private final BlockingQueue<IrcReplyEvent> serverRepliesQueue;
    private final BlockingQueue<IrcUserActionEvent> userActionsQueue;
    private final IrcEventFactory factory;
    private ServerWriter writer;
    private Reader reader;

    /**
     * Starts the thread
     *
     * @param serverRepliesQueue the queue with server replies
     * @param userActionsQueue the queue with action of other users
     * @param writer the server writer
     * @param reader the server reader
     */
    public CommunicationService(BlockingQueue<IrcReplyEvent> serverRepliesQueue,
                                BlockingQueue<IrcUserActionEvent> userActionsQueue,
                                ServerWriter writer,
                                Reader reader)
    {
        this.serverRepliesQueue = serverRepliesQueue;
        this.userActionsQueue = userActionsQueue;
        this.writer = writer;
        this.reader = reader;
        this.factory = new IrcEventFactory();

        final Thread serverSocketListener = new Thread(this, "SocketListeningThread");
        serverSocketListener.setDaemon(true);
        serverSocketListener.start();
        LOGGER.log(Level.INFO, "LISTENING TO SERVER");
    }


    public synchronized ServerWriter getWriter()
    {
        return this.writer;
    }

    /**
     * Listens for the server message, parse it to IrcEvent and distributes them to the appropriate {@link BlockingQueue}
     */
    @Override
    public void run()
    {
        String serverMessage;
        try
        {
            while ((serverMessage = reader.readLine()) != null)
            {
                LOGGER.log(Level.INFO, "<<< Received from server: " + serverMessage);
                IrcEvent event = factory.createEvent(serverMessage);
                if (event == null)
                    continue;

                try
                {
                    if (event instanceof IrcReplyEvent)
                        this.serverRepliesQueue.put((IrcReplyEvent) event);
                    else if (event instanceof IrcUserActionEvent)
                        this.userActionsQueue.put((IrcUserActionEvent) event);
                    else if (event instanceof PingIrcEvent)
                        writer.write("PONG " + event.getRaw());
                } catch (InterruptedException e)
                {
                    //This should happen only when the server is terminated
                    LOGGER.log(Level.SEVERE, "Server listening thread was interrupted ", e);
                }
            }
        } catch (IOException e)
        {
            if (e instanceof SocketException)
            {
                try
                {
                    //Do not remove this yet, unpredictable behavior needs testing
                    System.out.println("CONNECTION RESET, TRYING TO RECONNECT");
                    LOGGER.log(Level.INFO, "CONNECTION RESET, TRYING TO RECONNECT");
                    Connection.createConnection("irc.ubuntu.com", 6667);
                    reader = new Reader();
                    writer = new ServerWriter();
                } catch (IOException | IllegalAccessException ex)
                {
                    LOGGER.log(Level.SEVERE, "CONNECTION LOST", ex);
                    ex.printStackTrace();
                }

            } else
                LOGGER.log(Level.SEVERE, "CONNECTION LOST", e);
        }
        System.out.println("\n\n---------------- NO MORE LISTENING SERVER --------------------");
    }
}
