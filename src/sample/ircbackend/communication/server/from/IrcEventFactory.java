package sample.ircbackend.communication.server.from;


import sample.ircbackend.communication.server.from.events.api.IrcEvent;
import sample.ircbackend.communication.server.from.events.impl.user.action.IrcTopicChangedEvent;
import sample.ircbackend.communication.server.from.events.impl.PingIrcEvent;
import sample.ircbackend.communication.server.from.events.impl.reply.IrcErrorEvent;
import sample.ircbackend.communication.server.from.events.impl.reply.IrcNoticeReplyEvent;
import sample.ircbackend.communication.server.from.events.impl.reply.IrcReplyEvent;
import sample.ircbackend.communication.server.from.events.impl.user.IrcTopicWasChangedEvent;
import sample.ircbackend.communication.server.from.events.impl.user.action.*;
import sample.ircbackend.communication.server.to.CommandTranslator;
import sample.ircbackend.user.User;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Creates events from raw strings
 * using inner util classes {@link Parser} & {@link Parser.IRCMessage}
 * TODO: Maybe make into abstract factory with one factory for
 *  IrcReplyEvents and another for IrcUserAction events (and maybe for NOTICE events)
 */
public class IrcEventFactory
{
    private Parser.IRCMessage msg;

    public IrcEvent createEvent(String rawReply)
    {
        if (rawReply.split(" ")[0].equals("PING"))
            return new PingIrcEvent(rawReply.replace("PING", ""));


        Parser p = new Parser();
        this.msg = p.parse(rawReply);

        switch (msg.command)
        {
            case CommandTranslator.JOIN_CHANNEL_COMMAND:
            {
                User sender = extractUser(msg.prefix);
                return new IrcJoinEvent(sender, "", rawReply, msg.middle[0]);
            }
            case CommandTranslator.PRIVATE_MESSAGE_COMMAND:
            {
                User sender = extractUser(msg.prefix);
                return new IrcMessagedEvent(sender, msg.trailing, rawReply, msg.middle[0]);
            }
            case CommandTranslator.SET_NICK_COMMAND:
            {
                User sender = extractUser(msg.prefix);
                return new IrcNicknameUpdateEvent(sender, msg.trailing, rawReply, msg.trailing);
            }
            case CommandTranslator.TOPIC_COMMAND:
            {
                User user = extractUser(msg.prefix);
                return new IrcTopicChangedEvent(user, msg.trailing, msg.middle[0], rawReply);
            }
            case CommandTranslator.QUIT_CLIENT_SESSION_COMMAND:
            {
                User sender = extractUser(msg.prefix);
                return new IrcQuitEvent(sender, msg.trailing, rawReply, "");
            }
            case CommandTranslator.LEAVE_CHANNEL_COMMAND:
            {
                User sender = extractUser(msg.prefix);
                //middle[0] channel
                //middle[1] who kicked me
                return new IrcLeaveChannelEvent(sender, msg.trailing, rawReply, msg.middle[0]);
            }
            case CommandTranslator.KICK_USER:
            {
                User sender = extractUser(msg.prefix);
                //middle[0] channel
                //middle[1] who kicked me
                //trailing : reason
                return new IrcKickUserEvent(sender, msg.trailing, rawReply, msg.middle[1]);
            }
            case "NOTICE":
                return new IrcNoticeReplyEvent(rawReply);
            case "ERROR":
                return new IrcErrorEvent(msg.prefix, msg.command, msg.trailing, rawReply);
            default:
            {
                try
                {
                    Integer.parseInt(msg.command);
                } catch (NumberFormatException e)
                {
                    return new IrcNoticeReplyEvent(rawReply);
                }
                //Its not an user action its an integer reply code
                int replyCode = Integer.parseInt(msg.command);
                if (replyCode > 0 && replyCode < 100)
                {
                    return new IrcReplyEvent(msg.prefix, msg.command, msg.trailing, rawReply);
                } else if (replyCode >= 200 && replyCode < 400)
                {
                    //Success responses
                    return handleSuccessResponses(replyCode, rawReply);
                } else if (replyCode >= 400)
                {
                    //Error responses
                    return new IrcErrorEvent(msg.prefix, msg.command, msg.trailing, rawReply);
                }
            }
        }
        return new IrcNoticeReplyEvent(rawReply);
    }

    private IrcEvent handleSuccessResponses(int code, String rawReply)
    {
        switch (code)
        {
            case 301:
            {
                //TODO: UserIsAway
            }
            case 353:
            {
                List<String> usernames = Arrays.asList(msg.trailing.split(" "));
                List<User> usersList =
                        usernames.stream().map((username) -> {
                            User u = new User();
                            u.setOperator(username.startsWith("@"));
                            u.setNickname(username.replaceFirst("@", ""));
                            return u;
                        }).collect(Collectors.toList());
                return new IrcChannelUsersEvent(msg.raw, msg.middle[msg.middle.length - 1], usersList);
            }
            case 332:
            {
                User user = extractUser(msg.prefix);
                return new IrcTopicChangedEvent(user, msg.trailing, msg.middle[msg.middle.length - 1], rawReply);
            }
            case 333:
            {
                User user = extractUser(msg.middle[2]);
                String timestamp = msg.middle[3];
                LocalDateTime dateOfCreation = LocalDateTime
                        .ofInstant(Instant.ofEpochSecond(Long.parseLong(timestamp)),
                                TimeZone.getDefault().toZoneId());

                String channel = msg.middle[1];
                return new IrcTopicWasChangedEvent(user, dateOfCreation, channel, rawReply);
            }
            default:
                return new IrcNoticeReplyEvent(rawReply);
        }
    }

    private User extractUser(String prefix)
    {
        String username = prefix.split("!")[0];
        User u = new User();
        u.setNickname(username);
        u.setUsername(username);
        u.setFullName(username);

        return u;
    }

    /**
     * Parser object that parses the raw string into {@link IRCMessage} object
     * which is then used by the factory to make IrcEvents
     */
    private static class Parser
    {

        final IRCMessage parse(String input)
        {
            HashMap<String, String> message_tags = new HashMap<String, String>();
            String prefix = "";
            String command = "";
            ArrayList<String> middle = new ArrayList<String>();
            String trailing = "";

            //Invalid message
            if (input == null || input.equals(""))
                return null;

            //point to the next part that needs to be processed
            int cursor = 0;

            //Determine whether input start with @ (0x40)
            //Indicates that the message contains IRCv3.2 tags
            //Tags joined by ';' semicolon and ends at the first space
            if (input.charAt(0) == '@')
            {

                //The end of tags
                cursor = input.indexOf(" ");

                //Extract tags part excluding the @
                String tags = input.substring(1, cursor);

                //Break tags into tokens
                //Where Key=Value or Key without any value
                StringTokenizer token = new StringTokenizer(tags, ";");
                while (token.hasMoreTokens())
                {
                    //Extract Key and value
                    String[] kv = token.nextToken().split("=");
                    if (kv.length == 2)
                    {
                        String val = kv[1];
                        //Escape value
                        while (val.contains("\\r"))
                            val = val.replace("\\r", "\r");
                        while (val.contains("\\n"))
                            val = val.replace("\\n", "\n");
                        while (val.contains("\\\\"))
                            val = val.replace("\\\\", "\\");
                        while (val.contains("\\s"))
                            val = val.replace("\\s", " ");
                        while (val.contains("\\:"))
                            val = val.replace("\\:", ";");
                        message_tags.put(kv[0], val);

                    } else if (kv.length == 1)
                        message_tags.put(kv[0], null);
                }

            }

            //Ignore any whitespace
            while (input.charAt(cursor) == ' ')
                cursor++;

            //Determine whether message contains a prefix component
            //Prefix components starts with :
            if (input.charAt(cursor) == ':')
            {

                //Prefix beginning
                int prefix_beginning = cursor + 1;

                //Point at the next component
                cursor = input.indexOf(" ", cursor);

                //Get prefix
                prefix = input.substring(prefix_beginning, cursor);
            }

            //Ignore any whitespace
            while (input.charAt(cursor) == ' ')
                cursor++;

            //Extract the message command component
            //Command 1 letter or three digits
            if (cursor < input.length())
            {
                int command_beginning = cursor;
                cursor = input.indexOf(" ", cursor);
                command = input.substring(command_beginning, cursor);
            }

            //Ignore any whitespace
            while (input.charAt(cursor) == ' ')
                cursor++;

            //Extract message parameters component
            breakWhile:
            while (cursor < input.length())
            {
                int next = input.indexOf(" ", cursor);

                //Trailing part
                //End of message
                if (input.charAt(cursor) == ':')
                {
                    trailing = input.substring(++cursor, input.length());
                    break;
                }

                //Message contain trailing
                if (next != -1)
                {
                    middle.add(input.substring(cursor, next));
                    cursor = next + 1;

                    while (cursor < input.length() && input.charAt(cursor) == ' ')
                        cursor++;
                    continue;
                }

                //If message has no trailing
                //End of message
                if (next == -1)
                {
                    middle.addAll(Arrays.asList(input.substring(cursor, input.length()).split(" ")));
                    break breakWhile;
                }
            }

            return new IRCMessage(input, message_tags, prefix, command, middle.toArray(new String[middle.size()]), trailing);
        }

        /**
         * IRC message object used by the factory to create IrcEvents
         */
        private static class IRCMessage
        {

            /**
             * RAW Message
             */
            private final String raw;

            /**
             * IRCv3.2 Message Tags
             * <p>
             * additional and optional metadata included with relevant messages.
             * <p>
             * ABNF:
             * [ "@" tags SPACE ]
             * tags        =  tag *[ ";" tag ]
             * tag         =  key [ "=" value ]
             * key         =  [ vendor "/" ] 1*( ALPHA / DIGIT / "-" )
             * value       =  *valuechar
             * valuechar   =  %x01-06 / %x08-09 / %x0B-0C / %x0E-1F / %x21-3A / %x3C-FF
             * ; any octet except NUL, BELL, CR, LF, " " and ";"
             * <p>
             * <p>
             * Status:
             * Optional
             */
            private final HashMap<String, String> tags;

            /**
             * Message Prefix
             * <p>
             * The prefix is used by servers to indicate the true origin of a message.
             * <p>
             * ABNF:
             * prefix     =  servername / ( nickname [ [ "!" user ] "@" host ] )
             * <p>
             * Status:
             * Optional
             */
            private final String prefix;

            /**
             * Message Command
             * <p>
             * The command must either be a valid IRC command or a three-digit number represented as text.
             * <p>
             * ABNF:
             * command    =  1*letter / 3digit
             * <p>
             * Status:
             * Required
             */
            private final String command;

            /**
             * Message Parameters
             * <p>
             * Parameters (or ‘params’) are extra pieces of information added to the end of a message.
             * <p>
             * ABNF:
             * params     =  *14( SPACE middle ) [ SPACE ":" trailing ]
             * =/ 14( SPACE middle ) [ SPACE [ ":" ] trailing ]
             * <p>
             * Status:
             * Optional
             */
            private final String[] parameters;

            /**
             * Message Parameters Middle Part
             * <p>
             * ABNF:
             * nospcrlfcl *( ":" / nospcrlfcl )
             */
            private final String[] middle;

            /**
             * Message parameters Trailing part
             * *( ":" / " " / nospcrlfcl )
             */
            private final String trailing;

            /**
             * IRCMessage
             *
             * @param tags
             * @param prefix
             * @param command
             * @param middle
             * @param trailing
             */
            IRCMessage(String raw, HashMap<String, String> tags, String prefix, String command, String[] middle, String trailing)
            {
                this.raw = raw;
                this.tags = (tags == null) ? (HashMap<String, String>) Collections.<String, String>emptyMap() : tags;
                this.prefix = (prefix == null) ? "" : prefix;
                this.command = (command == null) ? "" : command;
                this.middle = (middle == null) ? new String[0] : middle;
                this.trailing = (trailing == null) ? "" : trailing;
                ArrayList<String> p = new ArrayList<String>(Arrays.asList(this.middle));
                p.add(this.trailing);
                this.parameters = p.toArray(new String[0]);
            }

            /**
             * Get raw message (Original before parsing)
             */
            public String getRaw()
            {
                return this.raw;
            }

            /**
             * Get message tags
             */
            public HashMap<String, String> getTags()
            {
                return this.tags;
            }

            /**
             * Get message prefix
             */
            public String getPrefix()
            {
                return this.prefix;
            }

            /**
             * Get message command
             */
            public String getCommand()
            {
                return this.command;
            }

            /**
             * Get message parameters
             */
            public String[] getParameters()
            {
                return this.parameters;
            }

            /**
             * Get message parameters middle part
             */
            public String[] getMiddle()
            {
                return this.middle;
            }

            /**
             * Get message parameters trailing part
             */
            public String getTrailing()
            {
                return this.trailing;
            }

        }
    }
}
