package sample.ircbackend.communication.server.from.events.impl.user.action;

import sample.ircbackend.communication.server.from.events.api.IrcEvent;
import sample.ircbackend.user.User;

import java.util.List;

/**
 * @author Stefan Ivanov
 */
public class IrcChannelUsersEvent extends AbstractIrcUserActionEvent
{
    private List<User> channelUsers;
    public IrcChannelUsersEvent(String rawMessage, String receiver, List<User> channelUsers)
    {
        super(new User(), IrcEventType.CHANNEL_USERS, "", rawMessage, receiver);
        this.channelUsers = channelUsers;
    }

    public List<User> getChannelUsers()
    {
        return channelUsers;
    }
}
