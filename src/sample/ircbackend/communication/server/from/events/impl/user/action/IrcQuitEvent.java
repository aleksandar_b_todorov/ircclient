package sample.ircbackend.communication.server.from.events.impl.user.action;

import sample.ircbackend.user.User;

public class IrcQuitEvent extends AbstractIrcUserActionEvent
{
    public IrcQuitEvent(User sender, String message, String rawMessage, String receiver)
    {
        super(sender, IrcEventType.QUITED, message, rawMessage, receiver);
    }
}
