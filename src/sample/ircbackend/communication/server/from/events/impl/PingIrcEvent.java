package sample.ircbackend.communication.server.from.events.impl;

import sample.ircbackend.communication.server.from.events.api.IrcEvent;

/**
 * @author Stefan Ivanov
 */
public class PingIrcEvent implements IrcEvent
{
    private final String pingContent;

    public PingIrcEvent(String pingContent)
    {
        this.pingContent = pingContent;
    }

    /**
     * Since its a PING from the server, the raw message will be the message without the PING command
     *
     * @return the ping content
     */
    @Override
    public String getRaw()
    {
        return this.pingContent;
    }
}
