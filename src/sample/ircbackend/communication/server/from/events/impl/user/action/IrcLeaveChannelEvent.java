package sample.ircbackend.communication.server.from.events.impl.user.action;

import sample.ircbackend.user.User;

public class IrcLeaveChannelEvent extends AbstractIrcUserActionEvent
{
    public IrcLeaveChannelEvent(User sender, String message, String rawMessage, String receiver)
    {
        super(sender, IrcEventType.LEFT, message, rawMessage, receiver);
    }
}
