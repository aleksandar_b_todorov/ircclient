package sample.ircbackend.communication.server.from.events.impl.user.action;

import sample.ircbackend.user.User;

public class IrcWhoIsEvent extends AbstractIrcUserActionEvent {
    public IrcWhoIsEvent(User sender, String message, String rawMessage, String receiver)
    {
        super(sender, IrcEventType.WHO_IS, message, rawMessage, receiver);
    }
}
