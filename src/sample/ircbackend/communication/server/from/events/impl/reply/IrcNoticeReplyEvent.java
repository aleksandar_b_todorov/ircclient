package sample.ircbackend.communication.server.from.events.impl.reply;

import sample.ircbackend.communication.server.from.events.api.IrcEvent;

/**
 * @author Stefan Ivanov
 */
public class IrcNoticeReplyEvent implements IrcEvent
{
    private String rawMessage;

    //TODO: Implements this constructor if needed.
    public IrcNoticeReplyEvent(String rawMessage)
    {
        this.rawMessage = rawMessage;
        //super(prefix, replyCode, message, rawMessage, IrcEventType.NOTICE);
    }

    @Override
    public String getRaw()
    {
        return this.rawMessage;
    }
}
