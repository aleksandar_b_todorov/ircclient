package sample.ircbackend.communication.server.from.events.impl.user.action;

import sample.ircbackend.user.User;

public class IrcKickUserEvent extends AbstractIrcUserActionEvent {
    public IrcKickUserEvent(User sender, String message, String rawMessage, String receiver) {
        super(sender, IrcEventType.KICKED, message, rawMessage, receiver);
    }
}
