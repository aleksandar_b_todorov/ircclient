package sample.ircbackend.communication.server.from.events.impl.reply;

import sample.ircbackend.communication.server.from.events.api.IrcReplyEvent;
import sample.ircbackend.communication.server.from.events.impl.user.action.IrcEventType;

/**
 * @author Stefan Ivanov
 */
@SuppressWarnings("WeakerAccess")
abstract class AbstractIrcReplyEvent implements IrcReplyEvent
{
    protected String prefix;
    protected String replyCode;
    protected String message;
    protected String rawMessage;
    protected IrcEventType type;

    public AbstractIrcReplyEvent(String prefix,
                                 String replyCode,
                                 String message,
                                 String rawMessage,
                                 IrcEventType type)
    {
        this.prefix = prefix;
        this.replyCode = replyCode;
        this.message = message;
        this.rawMessage = rawMessage;
        this.type = type;
    }

    @Override
    public String getPrefix()
    {
        return this.prefix;
    }

    @Override
    public String getReplyCode()
    {
        return this.replyCode;
    }

    @Override
    public String getMessage()
    {
        return this.message;
    }

    @Override
    public String getRaw()
    {
        return this.rawMessage;
    }

    @Override
    public IrcEventType getType()
    {
        return this.type;
    }
}
