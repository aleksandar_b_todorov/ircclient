package sample.ircbackend.communication.server.from.events.impl.user.action;

import sample.ircbackend.communication.server.from.events.api.IrcUserActionEvent;
import sample.ircbackend.user.User;

/**
 * @author Stefan Ivanov
 */
@SuppressWarnings("WeakerAccess")
public abstract class AbstractIrcUserActionEvent implements IrcUserActionEvent
{
    protected String rawMessage;

    protected User sender;
    protected IrcEventType userAction;
    protected String message;
    protected String receiver;

    public AbstractIrcUserActionEvent(User sender,
                                      IrcEventType userAction,
                                      String message,
                                      String rawMessage,
                                      String receiver)
    {
        this.sender = sender;
        this.userAction = userAction;
        this.message = message;
        this.rawMessage = rawMessage;
        //Its ok to be null
        this.receiver = receiver;
    }

    @Override
    public User getSender()
    {
        return this.sender;
    }

    @Override
    public IrcEventType getAction()
    {
        return this.userAction;
    }

    @Override
    public String getMessage()
    {
        return this.message;
    }

    @Override
    public String getRaw()
    {
        return this.rawMessage;
    }

    @Override
    public String getReceiver()
    {
        return this.receiver;
    }
}
