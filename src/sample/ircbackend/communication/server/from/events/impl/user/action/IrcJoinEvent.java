package sample.ircbackend.communication.server.from.events.impl.user.action;

import sample.ircbackend.user.User;

import java.util.ArrayList;
import java.util.List;

public class IrcJoinEvent extends AbstractIrcUserActionEvent
{
    public IrcJoinEvent(User sender, String message, String rawMessage, String receiver)
    {
        super(sender, IrcEventType.JOIN, message, rawMessage, receiver);
    }

}
