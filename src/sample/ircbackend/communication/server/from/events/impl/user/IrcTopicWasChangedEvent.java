package sample.ircbackend.communication.server.from.events.impl.user;

import sample.ircbackend.communication.server.from.events.impl.user.action.AbstractIrcUserActionEvent;
import sample.ircbackend.communication.server.from.events.impl.user.action.IrcEventType;
import sample.ircbackend.user.User;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author Stefan Ivanov
 */
public class IrcTopicWasChangedEvent extends AbstractIrcUserActionEvent
{
    private final LocalDateTime date;

    public IrcTopicWasChangedEvent(User user, LocalDateTime d, String channel, String rawReply)
    {
        super(user, IrcEventType.TOPIC_CHANGE_TIME, rawReply, rawReply, channel);
        this.date= d;
    }

    public LocalDateTime getDate()
    {
        return date;
    }
}
