package sample.ircbackend.communication.server.from.events.impl.user.action;

import sample.ircbackend.user.User;

public class IrcNicknameUpdateEvent extends AbstractIrcUserActionEvent {
    public IrcNicknameUpdateEvent(User sender, String message, String rawMessage, String receiver)
    {
        super(sender, IrcEventType.NICKNAME_UPDATE, message, rawMessage, receiver);
    }
}
