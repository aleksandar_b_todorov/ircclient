package sample.ircbackend.communication.server.from.events.impl.user.action;

/**
 * TODO: Finish prioritizing all events (if needed)
 * the lower the number the bigger the priority
 * @author Stefan Ivanov
 */
public enum IrcEventType
{
    REPLY(1),
    NOTICE(1),
    ERROR(0),
    JOIN(0),
    LEFT(1),
    QUITED(1),
    MESSAGED(1),
    WHO_IS(1),
    NICKNAME_UPDATE(1),
    CHANNEL_USERS(1),
    KICKED(1),
    TOPIC(1),
    TOPIC_CHANGE_TIME(1);

    public final int priority;

    IrcEventType(int priority)
    {
        this.priority = priority;
    }

}
