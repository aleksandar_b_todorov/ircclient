package sample.ircbackend.communication.server.from.events.impl.reply;

import sample.ircbackend.communication.server.from.events.impl.user.action.IrcEventType;

/**
 * @author Stefan Ivanov
 */
public class IrcReplyEvent extends AbstractIrcReplyEvent
{
    public IrcReplyEvent(String prefix, String replyCode, String message, String rawMessage)
    {
        super(prefix, replyCode, message, rawMessage, IrcEventType.REPLY);
    }
}
