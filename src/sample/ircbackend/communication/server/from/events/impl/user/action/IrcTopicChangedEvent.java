package sample.ircbackend.communication.server.from.events.impl.user.action;

import sample.ircbackend.user.User;

public class IrcTopicChangedEvent extends AbstractIrcUserActionEvent {
    public IrcTopicChangedEvent(User user, String newTopic, String channelName, String rawMessage) {
        super(user, IrcEventType.TOPIC, newTopic, rawMessage, channelName);
    }
}
