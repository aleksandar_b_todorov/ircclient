package sample.ircbackend.communication.server.from.events.impl.user.action;

import sample.ircbackend.user.User;

public class IrcMessagedEvent extends AbstractIrcUserActionEvent
{
    public IrcMessagedEvent(User sender, String message, String rawMessage, String receiver)
    {
        super(sender, IrcEventType.MESSAGED, message, rawMessage, receiver);
    }
}
