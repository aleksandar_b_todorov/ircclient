package sample.ircbackend.communication.server.from.events.api;

/**
 * Basic server message interface, every message from the server should be implementation of this interface
 *
 * @author Stefan Ivanov
 */
public interface IrcEvent
{
    /**
     * @return the unparsed raw message from the server
     */
    String getRaw();
}
