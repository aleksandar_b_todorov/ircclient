package sample.ircbackend.communication.server.from.events.api;

import sample.ircbackend.communication.server.from.events.impl.user.action.IrcEventType;
import sample.ircbackend.user.User;

/**
 * This is user action server message
 * Some user send you a message, or changed nickname or joined your channel
 *
 * @author Stefan Ivanov
 */
public interface IrcUserActionEvent extends IrcEvent
{
    /**
     * @return the user who initiated the action
     */
    User getSender();

    /**
     * @return the type of action
     */
    IrcEventType getAction();

    /**
     * The actual message can be info about the action the user did
     * Examples:
     *  QUIT :Remote host closed the connection
     *  NICK :{NEW_NICKNAME}
     * message is not always something the other user wrote, sometimes its server generated
     *
     * @return the message
     */
    String getMessage();

    /**
     * This method breaks interface segregation principle, and may be included inside {@link #getMessage}
     * Because not all user actions have a receiver
     * its only your nickname (private message)
     * or the channel name (channel message)
     *
     * @return the receiver
     */
    String getReceiver();
}
