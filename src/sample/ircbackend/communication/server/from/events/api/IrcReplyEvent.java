package sample.ircbackend.communication.server.from.events.api;

import sample.ircbackend.communication.server.from.events.impl.user.action.IrcEventType;

/**
 * This is a reply type message (client initiated an action and the server responds)
 *
 * @author Stefan Ivanov
 * @see {http://www.networksorcery.com/enp/protocol/irc.htm} If methods in this interface make no sense to you
 */
public interface IrcReplyEvent extends IrcEvent
{
    /**
     * Prefix is the first (optional) part of the message, starts with :
     * and in our case is info about the server
     *
     * @return the prefix
     */
    String getPrefix();

    /**
     * @return the reply code of the message
     */
    String getReplyCode();

    /**
     * @return the actual message
     */
    String getMessage();

    /**
     * @return the reply type (error or simple reply)
     */
    IrcEventType getType();
}
