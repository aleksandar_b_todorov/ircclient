package sample.ircbackend.communication.server.from;

import sample.ircbackend.connection.Connection;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Basic reader, uses BufferedReader to read the socket input stream
 */
public class Reader implements Closeable
{
    private static final Logger LOGGER = Logger.getGlobal();

    private BufferedReader reader;

    public Reader()
    {
        try
        {
            this.reader = new BufferedReader(
                    new InputStreamReader(Connection.getInputStream()));
        } catch (IOException | IllegalAccessException e)
        {
            LOGGER.log(Level.SEVERE, "Error getting the Socket's input stream, maybe connection not established", e);
        }
    }

    public String readLine() throws IOException
    {
        return reader.readLine();
    }

    @Override
    public void close() throws IOException
    {
        reader.close();
    }
}
