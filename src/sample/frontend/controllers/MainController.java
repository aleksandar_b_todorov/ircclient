package sample.frontend.controllers;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import sample.ircbackend.communication.server.CommunicationService;
import sample.ircbackend.communication.server.from.events.api.IrcReplyEvent;
import sample.ircbackend.communication.server.from.events.api.IrcUserActionEvent;
import sample.ircbackend.communication.server.from.events.impl.user.action.IrcEventType;
import sample.ircbackend.communication.server.to.CommandTranslator;
import sample.ircbackend.user.LoggedInUser;
import sample.ircbackend.user.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * @author Stefan Ivanov
 */
public class MainController
{
    private static final Logger LOGGER = Logger.getGlobal();

    private ExecutorService executor;
    private CommunicationService communicationService;
    private BlockingQueue<IrcReplyEvent> serverRepliesQueue;
    private BlockingQueue<IrcUserActionEvent> userActionsQueue;
    private ConcurrentMap<IrcEventType, List<IrcEventHandler>> registeredEventHandlers;

    @FXML
    public Button startButton;
    @FXML
    public ProgressIndicator loading;

    @FXML
    public Label errorLabel;

    @FXML
    public VBox initialView;

    @FXML
    public StackPane topContainer;

    @FXML
    public TextField username;


    /**
     * Initializes the variables
     * and stats the event dispatcher thread
     */
    public void init()
    {
        //This map wont be bigger than the length of IrcEventType enum values
        this.registeredEventHandlers = new ConcurrentHashMap<>(IrcEventType.values().length);
        this.executor = Executors.newCachedThreadPool();

        final Thread eventDispatcher = new Thread(getEventDispatcherTask(), "EventDispatcherThread");
        eventDispatcher.setDaemon(true);
        eventDispatcher.start();

        this.loading.setVisible(false);
        this.loading.setProgress(ProgressBar.INDETERMINATE_PROGRESS);

    }

    /**
     * registers this username (if available) and replace scene
     */
    @FXML
    public void startChatting()
    {
        String usernameText = username.getText();
        errorLabel.setVisible(false);

        //For now real name nickname and full name is all the same
        String command = CommandTranslator.login(usernameText, usernameText, usernameText);

        final Task<Void> dispatchCommandTask = getDispatchCommandTask(command);
        final Task<List<IrcReplyEvent>> waitForReplyTask = waitForReplyTask(false);


        this.startButton.disableProperty().bind(waitForReplyTask.runningProperty());
        this.loading.visibleProperty().bind(waitForReplyTask.runningProperty());
        this.executor.execute(dispatchCommandTask);
        this.executor.execute(waitForReplyTask);


        waitForReplyTask.setOnSucceeded((e) -> {
            final List<IrcReplyEvent> serverReplies = waitForReplyTask.getValue();
            //Since its for sure only 1 reply
            //This should be good enough
            if (serverReplies.get(0).getType() == IrcEventType.ERROR)
            {
                errorLabel.setVisible(true);
                errorLabel.setText(serverReplies.get(0).getMessage());
            } else
            {
                User user = new User();
                user.setNickname(usernameText);
                user.setUsername(usernameText);
                LoggedInUser.getInstance().setLoggedIn(true);
                LoggedInUser.getInstance().setLoggedInUser(user);

                FXMLLoader loader = new FXMLLoader(getClass().getResource("../fxmls/ChatBox.fxml"));
                Parent newParent;
                try
                {
                    newParent = loader.load();
                    topContainer.getChildren().add(newParent);

                    newParent.setScaleX(0);
                    newParent.setScaleY(0);
                    topContainer.getChildren().remove(initialView);
                    final Timeline timeline = new Timeline();
                    final KeyValue kv3 = new KeyValue(newParent.scaleXProperty(), 1, Interpolator.LINEAR);
                    final KeyValue kv4 = new KeyValue(newParent.scaleYProperty(), 1, Interpolator.LINEAR);
                    final KeyFrame kf = new KeyFrame(Duration.millis(1500), kv3, kv4);
                    timeline.getKeyFrames().add(kf);
                    timeline.play();

                    ChatController nextController = loader.getController();
                    nextController.setMainController(this);

                } catch (IOException ex)
                {
                    LOGGER.log(Level.SEVERE, "Could not load next scene " + ex);
                }
            }
        });
    }

    /**
     * Called when the UI was terminated, sends QUIT command to the server
     * and shutdowns the executor service ( and all the threads it handles)
     */
    public void terminate()
    {
        String quit = CommandTranslator.quitSession("User got disconnected");
        final Task<Void> dispatchCommandTask = getDispatchCommandTask(quit);
        executor.execute(dispatchCommandTask);
        dispatchCommandTask.setOnSucceeded((ev) -> executor.shutdownNow());
    }

    /**
     * Adds event handler for the desired event
     * Note: events have priority, so it doesnt matter the order in which
     * the handlers were registered
     *
     * @param event   the event type
     * @param handler the handler
     */
    void addEventHandler(IrcEventType event, IrcEventHandler handler)
    {
        //If there is no list at this event type, make new list and add the handler,
        //else add th handler to the existing list of event handlers
        this.registeredEventHandlers.merge(event, new LinkedList<IrcEventHandler>()
        {{
            add(handler);
        }}, (oldList, newList) -> {
            oldList.add(handler);
            return oldList;
        });

        LOGGER.info("Registered event handler for: " + event.toString());
    }

    /**
     * Waits for a reply to come in the {@link #serverRepliesQueue}
     * and returns it
     *
     * @param takeAll if this is true, it will take all the replies from the queue
     * @return the IrcReplyEvent from the queue
     */
    Task<List<IrcReplyEvent>> waitForReplyTask(boolean takeAll)
    {
        return new Task<List<IrcReplyEvent>>()
        {
            @Override
            protected List<IrcReplyEvent> call() throws Exception
            {
                List<IrcReplyEvent> replies = new LinkedList<>();
                //Will wait if the queue is empty
                IrcReplyEvent reply = serverRepliesQueue.take();
                replies.add(reply);
                if (takeAll)
                    while (!serverRepliesQueue.isEmpty())
                        replies.add(serverRepliesQueue.poll(2, TimeUnit.SECONDS));
                else
                    while (!serverRepliesQueue.isEmpty() &&
                            serverRepliesQueue.peek().getReplyCode().equals(reply.getReplyCode()))
                    {
                        replies.add(serverRepliesQueue.poll());
                    }
                return replies;
            }

            @Override
            protected void failed()
            {
                LOGGER.log(Level.WARNING, "Error getting server reply ", getException());
            }
        };
    }

    /**
     * Dispatches the desired command to the server
     *
     * @param command the command
     * @return the task
     */
    Task<Void> getDispatchCommandTask(String command)
    {
        return new Task<Void>()
        {
            @Override
            protected Void call()
            {
                communicationService.getWriter().write(command);
                return null;
            }

            @Override
            protected void failed()
            {
                LOGGER.log(Level.WARNING, "Error writing to server ", getException());
            }
        };
    }

    /**
     * Used by the {@link ChatController}
     *
     * @return the executor service
     */
    ExecutorService getExecutor()
    {
        return executor;
    }

    /**
     * get the event dispatcher,
     * basically every message that enters the {@link #userActionsQueue}
     * gets dispatched to everyone who subscribed to it (called {@link #addEventHandler(IrcEventType, IrcEventHandler)})
     * the user action queue is priority queue and events will be ordered by priority
     *
     * @return the Task
     */
    private Task<Void> getEventDispatcherTask()
    {
        return new Task<Void>()
        {
            @Override
            protected Void call() throws Exception
            {
                while (true)
                {
                    IrcUserActionEvent event = userActionsQueue.take();
                    List<IrcEventHandler> handlersToBeInvoked =
                            registeredEventHandlers.getOrDefault(event.getAction(), new ArrayList<>());

                    for (IrcEventHandler ircEventHandler : handlersToBeInvoked)
                    {
                        //Execute this on the main thread
                        Platform.runLater(() -> ircEventHandler.handle(event));
                    }
                }

            }

            @Override
            protected void failed()
            {
                LOGGER.log(Level.WARNING, "Error with the event dispatcher ", getException());
            }
        };
    }

    //GETTERS AND SETTERS

    public void setCommunicationService(CommunicationService communicationService)
    {
        this.communicationService = communicationService;
    }

    public void setServerRepliesQueue(BlockingQueue<IrcReplyEvent> serverRepliesQueue)
    {
        this.serverRepliesQueue = serverRepliesQueue;
    }

    public void setUserActionsQueue(BlockingQueue<IrcUserActionEvent> userActionsQueue)
    {
        this.userActionsQueue = userActionsQueue;
    }

}
