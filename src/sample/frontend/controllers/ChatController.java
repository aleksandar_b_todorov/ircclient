package sample.frontend.controllers;

import javafx.collections.transformation.FilteredList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sample.frontend.controllers.controls.channels.ChannelsView;
import sample.frontend.controllers.controls.chat.Chat;
import sample.frontend.controllers.controls.users.UserCell;
import sample.frontend.controllers.controls.users.UsersView;
import sample.ircbackend.communication.server.from.events.api.IrcReplyEvent;
import sample.ircbackend.communication.server.from.events.impl.user.action.IrcTopicChangedEvent;
import sample.ircbackend.communication.server.from.events.impl.reply.IrcErrorEvent;
import sample.ircbackend.communication.server.from.events.impl.user.IrcTopicWasChangedEvent;
import sample.ircbackend.communication.server.from.events.impl.user.action.*;
import sample.ircbackend.communication.server.to.CommandTranslator;
import sample.ircbackend.user.LoggedInUser;
import sample.ircbackend.user.User;

import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;


/**
 * The main working unit of the app
 * Uses mainController's tasks and executorService to execute commands to the server
 *
 * @author Stefan Ivanov
 */
public class ChatController {
    private static final Logger LOGGER = Logger.getGlobal();

    //Main controller reference for using of Server related tasks
    private MainController mainController;

    //For executing tasks(this is actually a reference to the main controller's executor service)
    private ExecutorService executorService;

    //Write messages here
    @FXML
    private TextArea messageBox;

    //Send message button
    @FXML
    private Button buttonSend;

    //Tabs container
    @FXML
    private TabPane tabs;

    //The inner container
    @FXML
    private HBox container;

    //Container for the users (right side)
    private VBox usersContainer;

    //ListView for the initial view (where only commands are accepted)
    private ListView<String> mainListView;

    //All opened chats key-> the chat name (channel or user)
    private Map<String, Chat> openedChats;

    //All opened chats, users key-> the channel name, value the list of users
    private Map<String, UsersView> openChatsUsers;

    //The left side channels view
    private ChannelsView channelsView;

    //The current open tab
    private String currentTab;


    /**
     * Initializes the controls, not managed by fxml file
     */
    @FXML
    private void initialize() {
        this.openedChats = new HashMap<>();
        this.openChatsUsers = new HashMap<>();
        this.container.setSpacing(15);

        URL style = getClass().getResource("../styles/tabs-style.css");
        tabs.getStylesheets().add(style.toExternalForm());

        currentTab = "Main";
        Tab tab = new Tab(currentTab);
        tab.setClosable(false);
        mainListView = new ListView<>();
        mainListView.setCellFactory(getCellFactory());
        mainListView.setPrefWidth(tabs.getWidth());
        mainListView.setPrefHeight(tabs.getHeight());
        tab.setContent(mainListView);
        tabs.getTabs().add(tab);


        //No users in the main view
        this.openChatsUsers.put(currentTab, null);

        messageBox.setPrefHeight(60);
        messageBox.setStyle("-fx-focus-color: #FFF;\n" +
                "-fx-faint-focus-color: #FFF;");


        alwaysWriteInMessageBox();

        this.channelsView = new ChannelsView(this.channelsViewActionHandler());
        channelsView.maxHeightProperty().bind(tabs.maxHeightProperty());
        channelsView.prefHeightProperty().bind(tabs.heightProperty());
        channelsView.setPrefWidth(200);
        channelsView.setMaxWidth(200);
        channelsView.setMinWidth(200);
        channelsView.setMinWidth(200);
        channelsView.setPadding(new Insets(31, 0, 0, 0));
        container.getChildren().add(0, channelsView);


        usersContainer = new VBox();

        usersContainer.prefHeightProperty().bind(tabs.heightProperty());

        usersContainer.setPadding(new Insets(33, 0, 0, 0));
        usersContainer.setPrefWidth(350);
        usersContainer.setMaxWidth(350);
        usersContainer.setOpacity(0.75);
        container.getChildren().add(usersContainer);
    }

    /**
     * Executed when the user press enter, or clicks the "Send message" button
     */
    @FXML
    public void sendButtonAction(ActionEvent actionEvent) {
        String txt = messageBox.getText();
        messageBox.setText("");
        if (txt.indexOf("/") == 0) {
            final Task<Void> dispatchCommandTask = mainController.getDispatchCommandTask(txt.replaceFirst("/", ""));
            final Task<List<IrcReplyEvent>> ircReplyEventTask = mainController.waitForReplyTask(true);

            ircReplyEventTask.setOnSucceeded((evHandler) ->
            {
                final List<IrcReplyEvent> replies = ircReplyEventTask.getValue();
                for (IrcReplyEvent reply : replies) {
                    if (currentTab.equalsIgnoreCase("MAIN"))

                        if (reply instanceof IrcErrorEvent)
                            mainListView.getItems().add("ERROR: " + reply.getMessage());
                        else
                            mainListView.getItems().add("SERVER: " + reply.getMessage());
                    else {
                        if (reply instanceof IrcErrorEvent) {
                            getOpenedChat().serverMessage(reply.getMessage(), true);
                        } else {
                            getOpenedChat().serverMessage(reply.getMessage(), false);
                        }
                    }

                }
            });

            executorService.execute(dispatchCommandTask);
            executorService.execute(ircReplyEventTask);
        } else {
            if (currentTab.equalsIgnoreCase("MAIN")) {
                mainListView.getItems().add("You are not in any channel, join a channel or message someone to send a message");
                return;
            }
            String receiver = currentTab;
            String command = CommandTranslator.privateMessage(receiver, txt);
            executeCommandWithoutReply(command, (evH) ->
            {
                openedChats.get(receiver).sendMessage(txt);
            }, null);
        }
    }

    /**
     * Executes the passed command and on attach on success and on fail listeners
     *
     * @param command   the command
     * @param onSuccess onSuccess event handler
     * @param onFail    onSuccess event handler
     */
    private void executeCommandWithoutReply(String command,
                                            EventHandler<WorkerStateEvent> onSuccess,
                                            @SuppressWarnings("SameParameterValue") EventHandler<WorkerStateEvent> onFail) {
        final Task<Void> dispatchCommandTask = mainController.getDispatchCommandTask(command);

        dispatchCommandTask.setOnSucceeded(onSuccess);
        if (onFail == null) {
            dispatchCommandTask.setOnFailed((e) -> LOGGER.log(Level.WARNING, "Error with dispatch task", dispatchCommandTask.getException()));
        } else
            dispatchCommandTask.setOnFailed(onFail);

        executorService.execute(dispatchCommandTask);

    }

    /**
     * Set the main controller reference and initializes fields
     *
     * @param mainController the main controller
     */
    void setMainController(MainController mainController) {
        this.mainController = mainController;
        executorService = mainController.getExecutor();

        final Task<List<IrcReplyEvent>> getInitialServerMessages = mainController.waitForReplyTask(true);
        getInitialServerMessages.setOnSucceeded((ev) ->
        {
            for (IrcReplyEvent ircReplyEvent : getInitialServerMessages.getValue()) {
                mainListView.getItems().add(ircReplyEvent.getPrefix() + " " + ircReplyEvent.getReplyCode() + " " + ircReplyEvent.getMessage());
            }
        });
        executorService.execute(getInitialServerMessages);

        addEventHandlers();
    }

    private void addEventHandlers() {
        this.mainController.addEventHandler(IrcEventType.MESSAGED, handleMessageEvent());

        this.mainController.addEventHandler(IrcEventType.JOIN, (e) -> {
            handleJoinEvent((IrcJoinEvent) e);
        });

        this.mainController.addEventHandler(IrcEventType.TOPIC_CHANGE_TIME, handleTopicChangedTimeEvent());

        this.mainController.addEventHandler(IrcEventType.CHANNEL_USERS, handleChannelUsersEvent());

        this.mainController.addEventHandler(IrcEventType.LEFT, handleUserLeftEvent());

        this.mainController.addEventHandler(IrcEventType.QUITED, handleUserQuitEvent());

        this.mainController.addEventHandler(IrcEventType.NICKNAME_UPDATE, handleNicknameChangeEvent());

        this.mainController.addEventHandler(IrcEventType.TOPIC, handleTopicChangedEvent());
    }

    private IrcEventHandler handleUserQuitEvent() {
        return e -> {
            IrcQuitEvent ev = (IrcQuitEvent) e;
            openChatsUsers.forEach((key, usersView) -> {
                if (usersView != null &&
                        usersView.containsUser(ev.getSender().getNickname())) {
                    usersView.removeUser(ev.getSender().getUsername());
                    openedChats.get(key).serverMessage(ev.getSender().getNickname() + " Has quit the server", false);
                }
            });

        };
    }

    private IrcEventHandler handleTopicChangedTimeEvent() {
        return e -> {
            IrcTopicWasChangedEvent ev = (IrcTopicWasChangedEvent) e;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY:mm:dd - HH:mm:ss");

            String formatDateTime = ev.getDate().format(formatter);
            if (this.openedChats.containsKey(ev.getReceiver())) {
                String serverMessage = String.format("topic was changed by %s on %s", ev.getSender().getNickname(), formatDateTime);
                this.openedChats.get(ev.getReceiver()).serverMessage(serverMessage, false);
            }
        };
    }

    /**
     * EventHandler for server message that someone changed channel topic
     */
    private IrcEventHandler handleTopicChangedEvent() {
        return (ev) -> {
            IrcTopicChangedEvent e = (IrcTopicChangedEvent) ev;

            this.openedChats.get(e.getReceiver()).setTopic(e.getMessage());
            this.openedChats.get(e.getReceiver()).serverMessage(e.getSender().getNickname() + " changed topic to " + e.getMessage(), false);
        };
    }

    /**
     * EventHandler for server message that someone changed his nick
     */
    private IrcEventHandler handleNicknameChangeEvent() {
        return (ev) -> {
            IrcNicknameUpdateEvent e = (IrcNicknameUpdateEvent) ev;
            boolean isSelf = false;
            final String newUsername = e.getReceiver();
            final String oldUsername = e.getSender().getNickname();
            if (isTheClient(e.getSender())) {
                LoggedInUser.getInstance().getLoggedInUser().setNickname(newUsername);
                isSelf = true;
            }

            if (currentTab.equalsIgnoreCase("MAIN") && isSelf)
                mainListView.getItems().add(oldUsername + " has changed nickname to " + newUsername);

            for (Map.Entry<String, UsersView> entry : this.openChatsUsers.entrySet()) {
                try {
                    UsersView entryValue = entry.getValue();
                    if (entryValue.containsUser(oldUsername)) {
                        entryValue.rename(oldUsername, newUsername);
                        String key = entry.getKey();
                        this.openedChats.get(key).serverMessage(oldUsername + " has changed his nickname to " + newUsername, false);
                    }
                } catch (NullPointerException ignored) {
                }
            }
        };
    }

    /**
     * Handle server message that some user has left some channel (PART)
     */
    private IrcEventHandler handleUserLeftEvent() {
        return (ev) -> {
            IrcLeaveChannelEvent e = (IrcLeaveChannelEvent) ev;
            String channel = e.getReceiver();

            FilteredList<Tab> tabOptional = tabs.getTabs().filtered((tap) -> tap.getText().equalsIgnoreCase(channel));

            if (!tabOptional.isEmpty() &&
                    isTheClient(e.getSender())) {
                Tab t = tabOptional.get(0);
                tabs.getTabs().remove(t);

                this.openedChats.remove(t.getText());
                this.openChatsUsers.remove(t.getText());
                this.channelsView.removeChannel(channel);
            } else {
                this.openChatsUsers.get(channel).removeUser(e.getSender().getNickname());
                this.openedChats.get(channel).serverMessage(e.getSender().getNickname() + " has left the channel", false);
            }
        };
    }

    /**
     * Handle the server message with users joined in the channel (353 reply code)
     */
    private IrcEventHandler handleChannelUsersEvent() {
        return (e) -> {
            IrcChannelUsersEvent ev = (IrcChannelUsersEvent) e;

            final String channelName = ev.getReceiver();

            if (openChatsUsers.containsKey(channelName)) {
                this.openChatsUsers.get(channelName).empty();
                this.openChatsUsers.get(channelName).addUsers(ev.getChannelUsers(), userDoubleClickedHandler());
            }
        };
    }

    /**
     * Handle JOIN message from the server
     * it could be the client or someone else has joined a channel
     *
     * @param e the event
     */
    private void handleJoinEvent(IrcJoinEvent e) {
        final String channelName = e.getReceiver();
        final User userWhoJoined = e.getSender();
        if (isTheClient(userWhoJoined)
                && !this.openedChats.containsKey(channelName)) {
            Tab tab = addNewTab(channelName);
            drawUsersList(channelName);
            tabs.getSelectionModel().select(tab);

            this.channelsView.addChannel(channelName);
        } else {
            Chat chat = this.openedChats.get(channelName);
            chat.serverMessage(userWhoJoined.getNickname() + "has joined the channel", false);
            this.openChatsUsers.get(channelName).addUser(userWhoJoined.getNickname(), userWhoJoined.isOperator(), userDoubleClickedHandler());
        }
    }

    /**
     * Check if the given user is the same as the logged in user
     *
     * @param sender user to check
     * @return true if the user is the logged in user
     */
    private boolean isTheClient(User sender) {
        return sender.getNickname().equalsIgnoreCase(LoggedInUser.getInstance().getLoggedInUser().getNickname());
    }

    /**
     * Event handling when the user rececives new message
     *
     * @return the EventHandler
     */
    private IrcEventHandler handleMessageEvent() {
        return (ev) ->
        {
            IrcMessagedEvent actualEvent = (IrcMessagedEvent) ev;
            final String senderNickname = actualEvent.getSender().getNickname();
            final String receiver = actualEvent.getReceiver();
            final String tabName;
            final boolean isLoggedInUser = senderNickname.equalsIgnoreCase(LoggedInUser.getInstance().getLoggedInUser().getNickname());

            if (receiver.startsWith("#") || receiver.startsWith("##") || receiver.startsWith("&")) {
                tabName = receiver;
            } else {
                tabName = senderNickname;
            }
            if (openedChats.containsKey(tabName)) {
                Chat thatView = openedChats.get(tabName);
                if (!tabName.equalsIgnoreCase(currentTab))
                    tabs.getTabs().forEach(t -> {
                        if (t.getText().equalsIgnoreCase(currentTab))
                            thatView.setStyle("-fx-background-color: #00ffffAA");
                    });
                thatView.receiveMessage(actualEvent.getMessage(), actualEvent.getSender().getNickname());
            } else {
                Tab newTab = addNewTab(tabName);
                //noinspection SpellCheckingInspection
                newTab.setStyle("-fx-background-color: #00FFFFAA");
                Chat listView = openedChats.get(tabName);
                listView.receiveMessage(actualEvent.getMessage(), actualEvent.getSender().getNickname());


                drawUsersList(tabName);
            }
        };
    }

    /**
     * Add new tab with the given text and returns it
     *
     * @param tabText the text of the tab
     * @return the tab
     */
    private Tab addNewTab(String tabText) {
        Tab tab = new Tab(tabText);

        Chat chat = new Chat(tabText, "");
        chat.prefHeightProperty().bind(tabs.heightProperty());
        tab.setContent(chat);
        //When a new tab is selected change the current tab and
        //show the new tab's list of users
        tabs.getSelectionModel().selectedItemProperty().addListener(
                (ov, t, newTab) -> {
                    currentTab = newTab.getText();
                    //Remove tab coloring if any
                    tab.setStyle("");

                    if (openChatsUsers.get(newTab.getText()) != null)
                        usersContainer.getChildren().setAll(openChatsUsers.get(newTab.getText()));
                    else
                        usersContainer.getChildren().clear();


                }
        );
        tab.setOnClosed((evH) -> {
            //Remove the chat from the list
            //If its a channel it will be only removed and the client wont part the channel
            openedChats.remove(tab.getText());
            openChatsUsers.remove(tab.getText());
        });

        drawUsersList(tabText);
        openedChats.put(tabText, chat);
        tabs.getTabs().add(tab);
        return tab;
    }

    /**
     * Get the current Chat
     *
     * @return the Chat associated with the selected tab
     */
    private Chat getOpenedChat() {
        return openedChats.get(currentTab);
    }

    /**
     * Draws the right control with the list of users
     */
    private void drawUsersList(String forChannel) {
        UsersView usersView = new UsersView();

        usersView.prefHeightProperty().bind(usersContainer.heightProperty());
        usersContainer.getChildren().setAll(usersView);


        this.openChatsUsers.put(forChannel, usersView);
    }

    /**
     * Callback for username double clicked in {@link UserCell} class (right users menu)
     */
    private UserCell.OnActionListener userDoubleClickedHandler() {

        return nickname -> {
            if (!openedChats.containsKey(nickname)) {
                Tab tab = addNewTab(nickname);
                tabs.getSelectionModel().select(tab);
            } else {
                //If there is a tab with that nickname, optional will always be present
                @SuppressWarnings("OptionalGetWithoutIsPresent") Tab tab = tabs.getTabs()
                        .stream()
                        .filter(
                                t -> t.getText()
                                        .equals(nickname))
                        .findFirst().get();
                tabs.getSelectionModel().select(tab);
            }
        };
    }

    /**
     * Handles actions from the {@link ChannelsView}
     *
     * @return the handler
     */
    private ChannelsView.OnAction channelsViewActionHandler() {
        return new ChannelsView.OnAction() {
            @Override
            public void joinChannel(String channelName) {
                if (!channelName.startsWith("#"))
                    channelName = "#" + channelName;
                String command = CommandTranslator.joinIntoChannel(channelName);
                executeCommandWithoutReply(command, null, null);
            }

            @Override
            public void openChannel(String channelName) {
                if (openedChats.containsKey(channelName)) {
                    currentTab = channelName;
                    Tab nextTab = tabs.getTabs().stream()
                            .filter(
                                    t ->
                                            t.getText().equalsIgnoreCase(channelName))
                            .collect(
                                    Collectors.toList())
                            .get(0);
                    tabs.getSelectionModel().select(nextTab);
                } else {
                    Tab nextTab = addNewTab(channelName);
                    tabs.getSelectionModel().select(nextTab);
                    executeCommandWithoutReply("NAMES " + channelName, null, null);
                    executeCommandWithoutReply("TOPIC " + channelName, null, null);
                }
            }

            @Override
            public void channelRemoved(String channelName) {
                String command = CommandTranslator.leaveChannel(channelName);
                executeCommandWithoutReply(command, null, null);
            }
        };
    }

    /**
     * Force the text written everywhere in the messagebox to be written in the textfield
     */
    private void alwaysWriteInMessageBox() {
        tabs.addEventHandler(KeyEvent.KEY_PRESSED, (e) ->
        {
            if (!messageBox.isFocused()) {
                messageBox.requestFocus();
            }
        });

        messageBox.addEventHandler(KeyEvent.KEY_PRESSED, (e) ->
        {
            if (e.getCode() == KeyCode.ENTER) {
                buttonSend.fire();
                e.consume();
            }
        });
    }

    /**
     * Make ERROR text red only in main view
     *
     * @return cell factory for the default listview (in Main tab)
     */
    private Callback<ListView<String>, ListCell<String>> getCellFactory() {
        return new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> param) {
                return new ListCell<String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);

                        if (item == null || empty) {
                            setText(null);
                            setStyle("-fx-control-inner-background: transparent");
                        } else {
                            setText(item);
                            if (item.contains("ERROR")) {
                                setStyle("-fx-control-inner-background: #ff4444");
                            } else {
                                setStyle("");
                            }
                        }
                    }
                };
            }
        };
    }
}
