package sample.frontend.controllers.controls.users;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import sample.ircbackend.Utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class UserCell extends BorderPane
{
    private Color DEFAULT_USER_COLOR = Color.valueOf("#000");
    private Color DEFAULT_OPERATOR_COLOR = Color.valueOf("#00ff22");

    private BorderPane layout;
    private Label usernameLabel;

    private final StringProperty username;
    private final boolean isOperator;
    private final OnActionListener callback;

    UserCell(String username, boolean isOperator, OnActionListener callback){
        this.username = new SimpleStringProperty(username);
        this.isOperator = isOperator;
        this.callback = callback;

        drawCell();
    }

    public BorderPane getLayout()
    {
        return layout;
    }

    private void drawCell()
    {
        this.layout = new BorderPane();
        this.usernameLabel = new Label();
        this.usernameLabel.setAlignment(Pos.BASELINE_CENTER);
        this.usernameLabel.setCursor(Cursor.HAND);
        this.usernameLabel.setFont(Font.font("Caveat", isOperator ? FontWeight.BOLD : FontWeight.NORMAL, 26));
        this.usernameLabel.setBackground(new Background(new BackgroundFill(Color.valueOf("#FFFFFFAA"), null, null)));
        this.usernameLabel.minWidthProperty().bind(widthProperty());
        this.usernameLabel.setWrapText(true);
        this.usernameLabel.textProperty().bind(username);
        final Tooltip labelToolTip = new Tooltip("Double click to open a chat with that user");
        labelToolTip.setFont(Font.font(12));
        Utils.hackTooltipStartTiming(labelToolTip);
        this.usernameLabel.setTooltip(labelToolTip);

        Color color = isOperator ? DEFAULT_OPERATOR_COLOR : DEFAULT_USER_COLOR;

        this.usernameLabel.setTextFill(color);
        this.layout.setCenter(this.usernameLabel);
        setOnDoubleClick();

        getChildren().setAll(this.layout);
    }

    private void setOnDoubleClick()
    {
        this.layout.addEventHandler(MouseEvent.MOUSE_CLICKED, (e) -> {
            if(e.getClickCount() == 2){
                callback.actionPerformed(this.username.getValue());
            }
        });
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserCell userCell = (UserCell) o;
        return username.getValue().equalsIgnoreCase(userCell.username.getValue());
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(username);
    }

    public String getUsername()
    {
        return this.username.getValue();
    }

    public void setUsername(String newUsername)
    {
        this.username.setValue(newUsername);
    }

    /**
     * Callback for the ChatController, when client double clicks on username
     */
    public interface OnActionListener
    {
        void actionPerformed(String nickname);
    }


}