package sample.frontend.controllers.controls.users;

import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import sample.ircbackend.user.User;

import java.util.List;

/**
 * @author Stefan Ivanov
 */
public class UsersView extends VBox
{
    //The top container (this)
    private VBox userCellsContainer;
    //For scrolling if users list is too big
    private ScrollPane scrollPane;

    //ObservableList of users for observing changes
    private ObservableList<Node> userCells;

    public UsersView()
    {
        super(5);
        this.userCells = FXCollections.observableArrayList();
        setupElements();
    }

    /**
     * Adds multiple users
     *
     * @param channelUsers the list of users
     */
    public void addUsers(List<User> channelUsers, UserCell.OnActionListener onActionListener)
    {
        channelUsers.forEach((user) ->
                this.addUser(user.getNickname(), user.isOperator(), onActionListener));
    }

    /**
     * Adds new usercell to the list
     *
     * @param username the username of the newly joined user
     */
    public void addUser(String username, boolean isOperator, UserCell.OnActionListener onActionListener)
    {
        this.userCells.add(new UserCell(username, isOperator, onActionListener));
    }

    /**
     * Remove user with username from the list
     * since {@link UserCell#equals(Object)} compares by username, passing new UserCell(username) will work
     *
     * @param username the username of the user that has left the channel
     */
    public void removeUser(String username)
    {
        this.userCells.remove(new UserCell(username, false, null));
    }

    private void setupElements()
    {
        this.userCellsContainer = new VBox(50);
        this.userCellsContainer.setPadding(new Insets(20, 0, 0, 0));
        this.scrollPane = new ScrollPane(userCellsContainer);
        this.scrollPane.setFitToWidth(true);

        this.scrollPane.getStyleClass().add("edge-to-edge");
        this.scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        this.scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        this.scrollPane.prefWidthProperty().bind(this.prefWidthProperty().subtract(5));
        this.scrollPane.prefHeightProperty().bind(this.heightProperty());

        Bindings.bindContentBidirectional(userCells, userCellsContainer.getChildren());

        getChildren().setAll(this.scrollPane);
        layout();
    }

    public boolean containsUser(String nickname)
    {
        return this.userCells.contains(new UserCell(nickname, false, null));
    }

    public void rename(String oldUsername, String newUsername)
    {
        final FilteredList<Node> filtered =
                this.userCells.filtered((cell) -> ((UserCell) cell).getUsername().equalsIgnoreCase(oldUsername));
        if (!filtered.isEmpty())
        {
            ((UserCell) filtered.get(0)).setUsername(newUsername);
        }

    }


    public void empty()
    {
        this.userCells.clear();
        layout();
    }
}
