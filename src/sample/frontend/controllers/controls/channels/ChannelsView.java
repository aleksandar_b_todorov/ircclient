package sample.frontend.controllers.controls.channels;

import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Ivanov
 */
public class ChannelsView extends AnchorPane
{
    private final OnAction callBack;

    private ObservableList<Node> channels;

    private VBox channelContainer;

    private ScrollPane channelScroller;

    private VBox joinChannelContainer;

    private TextField joinChannelInput;

    private Button joinChannelButton;

    public ChannelsView(OnAction callback)
    {
        super();
        this.callBack = callback;
        setupElements();
        layout();
    }

    private void setupElements()
    {
        this.setMaxHeight(768);
        this.setPrefHeight(768);
        //Channels instantiation
        this.channels = FXCollections.observableArrayList();

        //Channel container
        this.channelContainer = new VBox(10);

        //Channel scroller
        this.channelScroller = new ScrollPane(channelContainer);
        this.channelScroller.setFitToWidth(true);

        this.channelScroller.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        this.channelScroller.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        //join channel section
        this.joinChannelContainer = new VBox(5);
        this.joinChannelInput = new TextField();
        this.joinChannelButton = new Button("Join channel");
        this.joinChannelContainer.getChildren().setAll(joinChannelInput, joinChannelButton);
        this.joinChannelContainer.setPrefHeight(85);
        this.joinChannelContainer.setMaxHeight(85);

        joinChannelButton.getStyleClass().add("btn-3d");
        joinChannelButton.setOnMouseClicked((e) -> this.callBack.joinChannel(joinChannelInput.getText()));

        //Bindings
        Bindings.bindContentBidirectional(channels, channelContainer.getChildren());

        this.channelContainer.maxHeightProperty().bind(this.prefHeightProperty());
        this.channelScroller.prefHeightProperty().bind(this.heightProperty().subtract(joinChannelContainer.heightProperty()).add(40));
        this.channelScroller.maxHeightProperty().bind(this.heightProperty());
        this.channelContainer.prefWidthProperty().bind(this.widthProperty());
        this.joinChannelContainer.prefWidthProperty().bind(this.widthProperty());
        this.joinChannelInput.maxWidthProperty().bind(this.widthProperty());
        this.joinChannelButton.prefWidthProperty().bind(this.widthProperty());
        this.joinChannelButton.disableProperty().bind(joinChannelInput.textProperty().isEmpty());

        getChildren().setAll(channelScroller, joinChannelContainer);
        //Anchoring
        setTopAnchor(channelScroller, 2.5);
        setLeftAnchor(joinChannelContainer, 5.0);
        setRightAnchor(joinChannelContainer, 5.0);
        setBottomAnchor(joinChannelContainer, 15.0);


        this.addEventHandler(KeyEvent.KEY_PRESSED, e -> {
            if (!joinChannelInput.isFocused())
            {
                joinChannelInput.requestFocus();
                joinChannelInput.setText(joinChannelInput.getText() + e.getText());
            }
        });
    }

    public void addChannel(String channel)
    {
        this.channels.add(new ChannelCell(channel, callBack, this));
        joinChannelInput.clear();
    }

    public void removeChannel(String channel)
    {
        this.channels.removeAll(new ChannelCell(channel, null, null));
    }

    public interface OnAction
    {
        void joinChannel(String channelName);

        void openChannel(String channelName);

        void channelRemoved(String channelName);
    }
}
