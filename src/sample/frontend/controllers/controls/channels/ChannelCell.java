package sample.frontend.controllers.controls.channels;

import javafx.animation.FadeTransition;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;
import sample.ircbackend.Utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Objects;

/**
 * @author Borimir Georgiev
 */
public class ChannelCell extends HBox
{
    private final ChannelsView.OnAction callback;
    private final ChannelsView channelsView;
    private Label label;
    private Pane pane;
    private final FadeTransition fadeTransition = new FadeTransition(Duration.millis(700), this);


    private Button button;
    private String channelName;

    public ChannelCell(String channelName, ChannelsView.OnAction callBack, ChannelsView channelsView)
    {
        this.channelName = channelName;
        this.callback = callBack;
        this.channelsView = channelsView;
        setupElements();

        this.getChildren().setAll(label, button);
        HBox.setHgrow(pane, Priority.ALWAYS);
    }

    private void removeChannel()
    {
        fadeTransition.setFromValue(1.0);
        fadeTransition.setToValue(0.0);
        fadeTransition.play();
        fadeTransition.setOnFinished(e -> channelsView.removeChannel(this.channelName));
    }

    private void setupElements()
    {
        try
        {
            URL closeImagePath = getClass().getResource("../../../../resources/x.png");
            FileInputStream input = new FileInputStream(closeImagePath.getFile());
            Image image = new Image(input, 10, 10, true, true);
            ImageView imageView = new ImageView(image);

            this.button = new Button("", imageView);
            this.button.setCursor(Cursor.HAND);
            this.button.setOnMouseClicked(ev -> {
                removeChannel();
                callback.channelRemoved(this.channelName);
            });

            setDropShadowButtonEffect();
            setButtonStyle();

            this.pane = new Pane();
            this.label = new Label(this.channelName);
            this.label.setAlignment(Pos.BASELINE_CENTER);
            this.label.setTextAlignment(TextAlignment.CENTER);
            this.label.setPadding(new Insets(10, 15, 0, 10));
            this.label.setFont(Font.font("Caveat", 26));
            Tooltip tooltip = new Tooltip("Double click to start a chat with this channel");
            tooltip.setFont(Font.font(13));
            Utils.hackTooltipStartTiming(tooltip);
            this.label.setTooltip(tooltip);
            this.label.setCursor(Cursor.HAND);
            this.label.setOnMouseClicked((e)-> {
                if(e.getClickCount() == 2)
                    callback.openChannel(this.channelName);
            });
        } catch (FileNotFoundException e)
        {
            //TODO: replace with logger
            e.printStackTrace();
        }
    }

    private void setButtonStyle()
    {
        button.setStyle("-fx-padding: 20,0,0,0;" +
                "-fx-border-color: transparent;\n" +
                "    -fx-border-width: 0;\n" +
                "    -fx-background-radius: 0;\n" +
                "    -fx-background-color: transparent;\n" +
                "    -fx-font-family:\"Segoe UI\", Helvetica, Arial, sans-serif;\n" +
                "    -fx-font-size: 1.0em; /* 12 */\n" +
                "    -fx-text-fill: #828282;");
    }

    private void setDropShadowButtonEffect()
    {
        DropShadow shadow = new DropShadow();
        //Adding the shadow when the mouse cursor is on
        button.addEventHandler(MouseEvent.MOUSE_ENTERED,
                e -> button.setEffect(shadow));
        //Removing the shadow when the mouse cursor is off
        button.addEventHandler(MouseEvent.MOUSE_EXITED,
                e -> button.setEffect(null));
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChannelCell that = (ChannelCell) o;
        return channelName.equals(that.channelName);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(channelName);
    }
}
