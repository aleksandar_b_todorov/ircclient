package sample.frontend.controllers.controls.chat;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import sample.ircbackend.user.LoggedInUser;

import java.net.URL;

/**
 * @author Stefan Ivanov
 */
public class Chat extends VBox
{
    private StringProperty topic;
    private String conversationPartner;
    private ObservableList<Node> chatBubbles = FXCollections.observableArrayList();

    private StackPane chatContainer;
    private Label topicLabel;
    private ScrollPane messageScroller;
    private VBox messageContainer;

    public Chat(String channelOrNickname, String topic)
    {
        super(10);
        this.conversationPartner = channelOrNickname;
        this.topic = new SimpleStringProperty(topic);
        setupElements();
    }

    /**
     * Changes the topic of the chat
     * @param newTopic the new topic
     */
    public void setTopic(String newTopic){
        this.topic.setValue(newTopic);
    }

    private void setupElements()
    {
        setupContactHeader();
        setupMessageDisplay();
        getChildren().setAll(chatContainer);
        layout();
    }

    private void setupContactHeader()
    {
        topicLabel = new Label();
        topicLabel.textProperty().bind(topic);
        topicLabel.setStyle("-fx-background-color: #FFFFFF");
        topicLabel.setTextFill(Paint.valueOf("#000000BB"));
        topicLabel.setAlignment(Pos.TOP_CENTER);
        topicLabel.setWrapText(true);
        topicLabel.setFont(Font.font("Cormorant Infant", FontPosture.ITALIC, 26));
        topicLabel.prefWidthProperty().bind(widthProperty());
    }
    private void setupMessageDisplay()
    {
        chatContainer = new StackPane();
        messageContainer = new VBox(10);
        messageContainer.setStyle("-fx-padding: 40 0 0 0");


        Bindings.bindContentBidirectional(chatBubbles, messageContainer.getChildren());

        messageScroller = new ScrollPane(messageContainer);
        messageScroller.getStyleClass().add("edge-to-edge");
        messageScroller.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        messageScroller.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        messageScroller.prefWidthProperty().bind(messageContainer.prefWidthProperty().subtract(5));
        messageScroller.prefHeightProperty().bind(this.heightProperty());
        messageScroller.setFitToWidth(true);


        URL style = getClass().getResource("../../../styles/chat-box.css");
        getStylesheets().add(style.toExternalForm());


        chatContainer.getChildren().add(messageScroller);
        chatContainer.getChildren().add(topicLabel);
        chatContainer.setAlignment(Pos.TOP_CENTER);

        //Make the scroller scroll to the bottom when a new message is added
        chatBubbles.addListener((ListChangeListener<Node>) change -> {
            while (change.next())
            {
                if (change.wasAdded())
                {
                    try
                    {
                        Thread.sleep(100);
                    } catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }

                    messageScroller.layout();
                    messageScroller.setVvalue(Double.MAX_VALUE);
                }
            }
        });
    }

    public void sendMessage(String message)
    {
        chatBubbles.add(new ChatBubble(message, LoggedInUser.getInstance().getLoggedInUser().getNickname(), BubbleDirection.LEFT));
    }

    public void receiveMessage(String message, String sender)
    {
        chatBubbles.add(new ChatBubble(message, sender, BubbleDirection.RIGHT));
    }

    public void serverMessage(String message, boolean isError)
    {
        chatBubbles.add(new ChatBubble(message, isError));
    }
}
