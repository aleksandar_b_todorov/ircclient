package sample.frontend.controllers.controls.chat;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.SVGPath;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Stefan Ivanov
 */
enum BubbleDirection
{
    LEFT, RIGHT, CENTER
}
public class ChatBubble extends HBox
{
    private Color DEFAULT_SENDER_COLOR = Color.valueOf("#4285F4");
    private Color DEFAULT_RECEIVER_COLOR = Color.valueOf("#8701a8");
    private Color DEFAULT_SYSTEM_COLOR = Color.valueOf("#616161");
    private Color DEFAULT_SYSTEM_ERROR_COLOR = Color.valueOf("#ff4444");

    private Background DEFAULT_SENDER_BACKGROUND, DEFAULT_RECEIVER_BACKGROUND;

    private String message;
    private LocalDateTime time;
    private String nickname;

    private BubbleDirection direction;
    private boolean isError;

    private VBox senderAndTimeContainer;
    private HBox chatMessage;

    private Label timeSent;
    private Label senderNickname;
    private Label displayedText;
    private SVGPath directionIndicator;

    ChatBubble(String message, String nickname, BubbleDirection direction)
    {
        this.message = message;
        this.nickname = nickname;
        this.direction = direction;
        this.time = LocalDateTime.now();
        initialiseDefaults();
        setupElements();
    }

    /**
     * Server message constructor
     *
     * @param message the message
     * @param isError if the server message is error or notice
     */
    ChatBubble(String message, boolean isError)
    {
        this.message = message;
        this.direction = BubbleDirection.CENTER;
        this.nickname = "SERVER " + (isError ? "ERROR" : "");
        this.time = LocalDateTime.now();
        this.isError = isError;
        initialiseDefaults();
        setupElements();
    }

    private void initialiseDefaults()
    {
        DEFAULT_SENDER_BACKGROUND = new Background(
                new BackgroundFill(DEFAULT_SENDER_COLOR, new CornerRadii(7, 0, 7, 7, false), Insets.EMPTY));
        DEFAULT_RECEIVER_BACKGROUND = new Background(
                new BackgroundFill(DEFAULT_RECEIVER_COLOR, new CornerRadii(0, 7, 7, 7, false), Insets.EMPTY));
    }

    private void setupElements()
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        String formatDateTime = time.format(formatter);
        senderNickname = new Label(nickname);
        senderNickname.setFont(Font.font("Lobster", 19));
        timeSent = new Label(formatDateTime);
        timeSent.setTextFill(Color.color(0, 0, 0, 0.72));
        timeSent.setStyle("-fx-font-family: Cormorant Infant; -fx-font-size: 14");
        displayedText = new Label(message);
        displayedText.setPadding(new Insets(10));
        displayedText.setWrapText(true);
        displayedText.setStyle("-fx-font-family: Caveat; -fx-font-size: 30");
        directionIndicator = new SVGPath();

        senderAndTimeContainer = new VBox(senderNickname, timeSent);
        senderAndTimeContainer.setPadding(new Insets(5, 5, 5, 5));
        senderAndTimeContainer.setSpacing(5);
        senderAndTimeContainer.setMinWidth(80);
        senderAndTimeContainer.setMaxHeight(80);
        displayedText.minHeightProperty().bind(senderAndTimeContainer.heightProperty());


        if (direction == BubbleDirection.LEFT)
        {
            configureForReceiver();
        } else if (direction == BubbleDirection.RIGHT)
        {
            configureForSender();
        } else
        {
            configureForServer();
        }
    }

    private void configureForSender()
    {
        displayedText.setAlignment(Pos.CENTER_RIGHT);
        directionIndicator.setContent("M10 0 L0 10 L0 0 Z");
        directionIndicator.setFill(DEFAULT_SENDER_COLOR);

        chatMessage = new HBox(displayedText, senderAndTimeContainer);
        chatMessage.setOpacity(0.9);
        chatMessage.setBackground(DEFAULT_SENDER_BACKGROUND);

        senderNickname.setContentDisplay(ContentDisplay.RIGHT);
        senderNickname.setTextAlignment(TextAlignment.RIGHT);
        senderAndTimeContainer.setPadding(new Insets(5, 5, 5, 20));
        HBox container = new HBox(chatMessage, directionIndicator);

        //Use at most 75% of the width provided to the SpeechBox for displaying the message
        container.maxWidthProperty().bind(widthProperty().multiply(0.75));

        getChildren().setAll(container);

        setAlignment(Pos.CENTER_RIGHT);
    }

    private void configureForReceiver()
    {
        displayedText.setAlignment(Pos.CENTER_LEFT);
        directionIndicator.setContent("M0 0 L10 0 L10 10 Z");
        directionIndicator.setFill(DEFAULT_RECEIVER_COLOR);

        chatMessage = new HBox(senderAndTimeContainer, displayedText);
        chatMessage.getStyleClass().add("other-player");
        chatMessage.setOpacity(0.9);
        chatMessage.setBackground(DEFAULT_RECEIVER_BACKGROUND);
        HBox container = new HBox(directionIndicator, chatMessage);

        //Use at most 75% of the width provided to the SpeechBox for displaying the message
        container.maxWidthProperty().bind(widthProperty().multiply(0.75));

        getChildren().setAll(container);
        setAlignment(Pos.CENTER_LEFT);
    }

    private void configureForServer()
    {
        displayedText.setAlignment(Pos.BASELINE_CENTER);
        displayedText.setTextFill(Paint.valueOf("#000000CC"));
        displayedText.setTextFill(isError ? DEFAULT_SYSTEM_ERROR_COLOR : DEFAULT_SYSTEM_COLOR);
        displayedText.setPadding(new Insets(0));

        senderNickname.setTextFill(isError ? DEFAULT_SYSTEM_ERROR_COLOR : DEFAULT_SYSTEM_COLOR);
        senderNickname.setAlignment(Pos.BASELINE_CENTER);
        VBox container = new VBox(senderNickname, displayedText);

        container.setStyle("-fx-background-color: #FFFFFFAA");
        container.prefWidthProperty().bind(widthProperty());
        container.setAlignment(Pos.CENTER);

        getChildren().setAll(container);
        setAlignment(Pos.BASELINE_CENTER);
    }
}
