package sample.frontend.controllers;

import sample.ircbackend.communication.server.from.events.api.IrcEvent;

/**
 *
 * Event handling interface
 * Dont know if this should be in communication package
 * @author Stefan Ivanov
 */
public interface IrcEventHandler
{
    void handle(IrcEvent event);
}
