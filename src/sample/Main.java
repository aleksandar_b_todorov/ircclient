package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.frontend.controllers.MainController;
import sample.ircbackend.communication.server.CommunicationService;
import sample.ircbackend.communication.server.from.Reader;
import sample.ircbackend.communication.server.from.events.api.IrcReplyEvent;
import sample.ircbackend.communication.server.from.events.api.IrcUserActionEvent;
import sample.ircbackend.communication.server.to.ServerWriter;
import sample.ircbackend.connection.Connection;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.NoSuchFileException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Main extends Application
{
    private MainController mainController;

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("frontend/fxmls/InitialScene.fxml"));
        Parent root = loader.load();

        URL url = getClass().getResource("resources/bg.jpg");
        URL buttonsStyleUrl = getClass().getResource("frontend/styles/button-style.css");

        root.getStylesheets().add(buttonsStyleUrl.toExternalForm());

        root.setStyle("-fx-background-image: url('" + url.toExternalForm() + "'); " +
                "-fx-background-position: center center; " +
                "-fx-background-repeat: no-repeat;\n" +
                "-fx-background-size: cover, auto;");

        primaryStage.setTitle("MachineTeamChat");
        final Scene scene = new Scene(root, 1280, 768);

        scene.getStylesheets().add("https://fonts.googleapis.com/css?family=Lobster&display=swap&subset=cyrillic,cyrillic-ext");
        scene.getStylesheets().add("https://fonts.googleapis.com/css?family=Cormorant+Infant:400i,500i&display=swap&subset=cyrillic,cyrillic-ext");
        scene.getStylesheets().add("https://fonts.googleapis.com/css?family=Caveat&display=swap&subset=cyrillic,cyrillic-ext");

        primaryStage.setScene(scene);
        primaryStage.show();


        ServerWriter serverWriter = new ServerWriter();
        Reader serverReader = new Reader();
        BlockingQueue<IrcReplyEvent> ircReplyEventBlockingQueue = new LinkedBlockingDeque<>();
        BlockingQueue<IrcUserActionEvent> ircUserActionEventBlockingQueue =
                new PriorityBlockingQueue<>(100, Comparator.comparingInt(ev -> ev.getAction().priority));


        CommunicationService communicationService = new CommunicationService(
                ircReplyEventBlockingQueue,
                ircUserActionEventBlockingQueue,
                serverWriter,
                serverReader);

        mainController = loader.getController();
        mainController.setCommunicationService(communicationService);
        mainController.setUserActionsQueue(ircUserActionEventBlockingQueue);
        mainController.setServerRepliesQueue(ircReplyEventBlockingQueue);
        mainController.init();

    }

    @Override
    public void stop() throws Exception
    {
        Logger.getGlobal().log(Level.INFO, "App terminated");
        mainController.terminate();
        super.stop();
    }

    /**
     * Instantiate the connection when the app starts
     */
    public static void main(String[] args)
    {

        try
        {
            configureLogger();
            Connection.createConnection("irc.ubuntu.com", 6667);

            Logger.getGlobal().log(Level.INFO, "Connection established");
            launch(args);
        } catch (IOException | IllegalAccessException e)
        {
            Logger.getGlobal().log(Level.SEVERE, "Error establishing connection");
        }

    }

    private static void configureLogger() throws IOException
    {
        Logger globalLogger = Logger.getGlobal();

        String timeOfCreation = new SimpleDateFormat("EEE-hh-mm").format(new Date());

        String logFilePath = String.format("logs/%s_logs.txt", timeOfCreation);
        FileHandler handler;
        try
        {
            handler = new FileHandler(logFilePath, 1024 * 1024 * 10, 1, true);
        } catch (NoSuchFileException e)
        {
            String parentName = new File(logFilePath).getParent();
            if (parentName != null)
            {
                File parentDir = new File(parentName);
                if (!parentDir.exists() && parentDir.mkdirs())
                {
                    handler = new FileHandler(logFilePath, 1024 * 1024 * 10, 1, true);
                } else
                {
                    throw e;
                }
            } else
            {
                throw e;
            }
        }

        globalLogger.addHandler(handler);
        SimpleFormatter formatter = new SimpleFormatter();
        handler.setFormatter(formatter);
        globalLogger.setUseParentHandlers(false);
    }
}
